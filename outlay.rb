#!/usr/bin/ruby
# coding: utf-8 

Encoding.default_external = "UTF-8"

require 'net/https'
require 'uri'
require 'json'
require 'sqlite3'

module Outlay

def self.getList(month_str,word="")
  if month_str.instance_of?(String) && /^201\d-[01]\d$/ =~ month_str
    db = SQLite3::Database.new(File.dirname(__FILE__) + "/outlay.db")
    escaped = word.gsub(/'/,"''")
    sql_str = "select * from outlaylist where (date like '#{month_str}-__') and (nominal like '%#{escaped}%') order by date asc;"# + month_str + "-__';"
    db.execute(sql_str)
  else
    []
  end
end

def self.getItem(id)
  if id.integer? #.instance_of?(Integer)
    db = SQLite3::Database.new(File.dirname(__FILE__) + "/outlay.db")
    sql_str = "select * from outlaylist where id=#{id};"
    db.execute(sql_str)
  else
    []
  end
end

def self.deleteItem(id)
  if id.integer? #.instance_of?(Integer)
    db = SQLite3::Database.new(File.dirname(__FILE__) + "/outlay.db")
    sql_str = "delete from outlaylist where id=#{id};" # + id + ";"
    db.execute(sql_str)
    true
  else
    false
  end
end

def self.setItem(date_str,nominal_str,price)
#  print File.dirname(__FILE__) + "/outlay.db"
  if (date_str.instance_of?(String) && 
      nominal_str.instance_of?(String) && 
      price.integer? &&
      /^201\d-[01]\d-[0-3]\d$/ =~ date_str && price > 0)
    escaped_str = nominal_str.gsub(/'/,"''")    
    db = SQLite3::Database.new(File.dirname(__FILE__) + "/outlay.db")
    sql_str = "insert into outlaylist(date,nominal,price) values('#{date_str}','#{escaped_str}',#{price});"
    db.execute(sql_str)
    true
  else
    false
  end
end

end



#p setItem("2015-03-05","book's",500)
#p deleteItem(6)
#p JSON.fast_generate(getList("2015-03"))
#p Outlay.getList("2015-04","")
