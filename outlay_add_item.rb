#!/usr/bin/ruby
# coding: utf-8

Encoding.default_external = "UTF-8"

require 'json'
require "cgi"
require_relative 'outlay'

cgi = CGI.new
print "Content-type: application/json\n\n"

date_str = cgi["date"]
nominal_str = cgi["nominal"]
price = cgi["price"].to_i

if(Outlay.setItem(date_str,nominal_str,price))
  /^(201\d-[01]\d)-[0-3]\d$/ =~ date_str
  list = Outlay.getList($~[1])
  print(JSON.fast_generate(list))
else
  print("[]")
end
