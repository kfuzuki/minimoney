#!/usr/bin/ruby
# coding: utf-8

Encoding.default_external = "UTF-8"

require 'json'
require 'qreki'
require "cgi"

cgi = CGI.new
print "Content-type: application/json\n\n"
#print "{\"test\"=2001}"
#exit()

y = cgi["y"].to_i
m = cgi["m"].to_i
d = cgi["d"].to_i

q = nil
if y > 1900 && m > 0 && m < 13 && d > 0 && d < 32
  q = Qreki.calc_from_date(Date.new(y, m, d))
else
  q = Qreki.calc_from_date(Date.today)
end
#puts "旧暦#{q.year}年#{q.uruu ? '閏' : nil}#{q.month}月#{q.day}日(#{q.rokuyou})#{q.sekki}"
qhash = Hash.new
qhash.store('year',q.year)
qhash.store('month',q.month)
qhash.store('day',q.day)
qhash.store('uruu',q.uruu)
qhash.store('rokuyou',q.rokuyou)
qhash.store('sekki',q.sekki)

print JSON.fast_generate(qhash)
