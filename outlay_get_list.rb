#!/usr/bin/ruby
# coding: utf-8

Encoding.default_external = "UTF-8"

require 'json'
require "cgi"
require_relative 'outlay'

cgi = CGI.new
print "Content-type: application/json\n\n"

month = cgi["month"]
word = cgi["word"]
print JSON.fast_generate(Outlay.getList(month,word))
