#!/usr/bin/ruby
# coding: utf-8

Encoding.default_external = "UTF-8"

require 'json'
require 'cgi'
require_relative 'outlay'

cgi = CGI.new
print "Content-type: application/json\n\n"

id = cgi["id"].to_i

if id > 0
  item = Outlay.getItem(id)
  if item.size == 1 && Outlay.deleteItem(id)
      /^(201\d-[01]\d)-[0-3]\d$/ =~ item[0][1]
     list = Outlay.getList($~[1])
     print(JSON.fast_generate(list))
  else
    print "[]"
  end
else
  print "[]"
end

